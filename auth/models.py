from django.db import models
from django.core.exceptions import ObjectDoesNotExist
import unittest


# Create your models here.

SUCCESS               =   1
ERR_BAD_CREDENTIALS   =  -1
ERR_USER_EXISTS       =  -2
ERR_BAD_USERNAME      =  -3
ERR_BAD_PASSWORD      =  -4

class MyTestCase(unittest.TestCase):
    def setUp(self):
	self.jenni = UsersModel.objects.create(user="jenni", password="11111", count=1)
	self.joyce = UsersModel.objects.create(user="joyce", password="yyy22", count=1)

    def testLoginSuccess(self):
   	self.assertEquals(UsersModel.objects.login("jenni", "11111"), UsersModel.objects.get(user="jenni").count)
   	self.assertEquals(UsersModel.objects.login("jenni", "11111"), UsersModel.objects.get(user="jenni").count)
   	self.assertEquals(UsersModel.objects.login("joyce", "yyy22"), UsersModel.objects.get(user="joyce").count)

    def testLoginNonExsitingUser(self):
   	self.assertEquals(UsersModel.objects.login("random1", "1234"), ERR_BAD_CREDENTIALS)
   	self.assertEquals(UsersModel.objects.login("random2", "5678"), ERR_BAD_CREDENTIALS)

    def testLoginEmptyUsername(self):
   	self.assertEquals(UsersModel.objects.login("", "1234"), ERR_BAD_CREDENTIALS)

    def testLoginLongUsername(self):
   	self.assertEquals(UsersModel.objects.login("jjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjj", "1234"), ERR_BAD_CREDENTIALS)

    def testLoginBadPwd(self):
   	self.assertEquals(UsersModel.objects.login("jenni", "xxx"), ERR_BAD_CREDENTIALS)
   	self.assertEquals(UsersModel.objects.login("jenni", ""), ERR_BAD_CREDENTIALS)
   	self.assertEquals(UsersModel.objects.login("joyce", "yyy"), ERR_BAD_CREDENTIALS)

    def testAddBadUsername(self):
	self.assertEquals(UsersModel.objects.add("", "anything"), ERR_BAD_USERNAME)
	self.assertEquals(UsersModel.objects.add("jjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjkkkkkkkkkkkkkkkkkkkkkkkkkkkk", "anything"), ERR_BAD_USERNAME)

    def testAddBadPassword(self):
	self.assertEquals(UsersModel.objects.add("whoever","jjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk"), ERR_BAD_PASSWORD)

    def testAddExsitUser(self):
   	self.assertEquals(UsersModel.objects.add("jenni", "11111"), ERR_USER_EXISTS)
   	self.assertEquals(UsersModel.objects.add("joyce", "yyy22"), ERR_USER_EXISTS)
    
    def testAddSuccess(self):
	self.assertEquals(UsersModel.objects.add("felicia", "x33333x"), UsersModel.objects.get(user="felicia").count)
   	self.assertEquals(UsersModel.objects.add("miki", "00000"), UsersModel.objects.get(user="felicia").count)

    def testRest(self):
   	self.assertEquals(UsersModel.objects.testAPT_resetFixture(), SUCCESS)

    def tearDown(self):
	self.jenni.delete()
	self.joyce.delete()


class UsersManager(models.Manager):
    def login(self, input_user, input_password):
	try:
	    temp_user = self.get(user = input_user)
	except ObjectDoesNotExist:
	    return ERR_BAD_CREDENTIALS
	if temp_user.password != input_password:
	    return ERR_BAD_CREDENTIALS
	
	temp_user.count += 1
	temp_user.save()
	return temp_user.count	

    def add(self, input_user, input_password):
	try:
	    temp_user = self.get(user = input_user)
	except ObjectDoesNotExist:
	    if input_user == "" or len(input_user) >= 128:
		return ERR_BAD_USERNAME
	    if len(input_password) >= 128:
		return ERR_BAD_PASSWORD
	    else:
		new_user = self.create(user = input_user, password = input_password, count = 1)
		return new_user.count
	return ERR_USER_EXISTS

    def testAPT_resetFixture(self):
	self.all().delete()
	return SUCCESS


class UsersModel(models.Model):
    user = models.CharField(max_length=128)
    user.primary_key = True
    password = models.CharField(max_length=128)
    count = models.IntegerField()
    objects = UsersManager()

    def __unicode__(self):
	return u'%s %s %s' % (self.user, self.password, self.count)

