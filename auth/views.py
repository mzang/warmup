import json
from django.shortcuts import render_to_response
from django.http import HttpResponse, Http404
from django.template import RequestContext
from auth.models import UsersModel
from auth.models import MyTestCase
from django.views.decorators.csrf import csrf_exempt
import StringIO
import unittest

SUCCESS               =   1
ERR_BAD_CREDENTIALS   =  -1
ERR_USER_EXISTS       =  -2
ERR_BAD_USERNAME      =  -3
ERR_BAD_PASSWORD      =  -4

@csrf_exempt
def main(request):
        if request.method == 'GET':
                return render_to_response('client.html',  context_instance=RequestContext(request))
        elif request.method =='POST':
                return HttpResponse('<h1>Wrong URL</h1>')

@csrf_exempt
def login(request):
    if request.method == "POST":
	rdata = json.loads(request.body)
	username = rdata['user']
	password = rdata['password']
	#if username is None or password is None:
	#    return 
	rval = UsersModel.objects.login(username, password)
	if rval < 0:
	    resp = {"errCode": rval}
	else:
	    resp = {"errCode": SUCCESS, "count": rval}
	data=json.dumps(resp)
	return HttpResponse(data, content_type="application/json")
    elif request.method == 'GET':
	return HttpResponse('<h1>Login Page</h1>') 
    else:
	raise Http404

@csrf_exempt
def add(request):
    if request.method == "POST":
	rdata = json.loads(request.body)
	username = rdata['user']
	password = rdata['password']
	if username is None or password is None:
	    return 
	rval = UsersModel.objects.add(username, password)
	if rval < 0:
	    resp = {"errCode": rval}
	else:
	    resp = {"errCode": SUCCESS, "count": rval}
	data=json.dumps(resp)
	return HttpResponse(data, content_type="application/json")
    elif request.method == "GET":
	return HttpResponse('<h1>Add Page</h1>')

@csrf_exempt
def reset(request):
    if request.method == "POST": 
        rval = UsersModel.objects.testAPT_resetFixture()
	resp = {"errCode": rval}
	data=json.dumps(resp)
	return HttpResponse(data, content_type="application/json")
    elif request.method == "GET":
	return HttpResponse('<h1>Reset Page</h1>')

@csrf_exempt
def unitTest(request):
    if request.method == "POST": 
	stBuffer = StringIO.StringIO()
	suite = unittest.TestLoader().loadTestsFromTestCase(MyTestCase)
	result = unittest.TextTestRunner(stream = stBuffer, verbosity = 2).run(suite)
	retVal = {"output": stBuffer.getvalue(), "totalTests": result.testsRun, "nrFailed": len(result.failures)}
	data = json.dumps(retVal)
	return HttpResponse(data, content_type="application/json")
    elif request.method == "GET":
	return HttpResponse('<h1>UnitTest Page </h1>')
