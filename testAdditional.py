import unittest
import os
import testLib


class TestAdditional(testLib.RestTestCase):

    def assertResponse(self, respData, count = 1, errCode = testLib.RestTestCase.SUCCESS):
        expected = { 'errCode' : errCode }
        if count is not None:
            expected['count']  = count
        self.assertDictEqual(expected, respData)


    def testEmptyUsername(self):
        respData = self.makeRequest("/users/add", method="POST", data = { 'user' : '', 'password' : 'password'} )
        self.assertResponse(respData, count = None, errCode = testLib.RestTestCase.ERR_BAD_USERNAME)

    def testLongUsername(self):
        respData = self.makeRequest("/users/add", method="POST", data = { 'user' : 'kkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk', 'password' : 'password'} )
        self.assertResponse(respData, count = None, errCode = testLib.RestTestCase.ERR_BAD_USERNAME)

    def testLongPassword(self):
        respData = self.makeRequest("/users/add", method="POST", data = { 'user': 'user3', 'password' : 'kkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk'} )
        self.assertResponse(respData, count = None, errCode = testLib.RestTestCase.ERR_BAD_PASSWORD)


    def testExsitUsername(self):
	self.makeRequest("/users/add", method = "POST", data = {'user':"cassie", 'password':"password", 'count':1})
        respData = self.makeRequest("/users/add", method="POST", data = { 'user' : 'cassie', 'password' : 'password'} )
        self.assertResponse(respData, count = None, errCode = testLib.RestTestCase.ERR_USER_EXISTS)

    def testLogin(self):
	self.makeRequest("/users/add", method = "POST", data = {'user':"jesse", 'password':"1234", 'count':1})
        respData = self.makeRequest("/users/login", method="POST", data = { 'user' : 'jesse', 'password' : '1234'} )
        self.assertResponse(respData, count =2)

    def testLoginBadCredential(self):
	self.makeRequest("/users/add", method = "POST", data = {'user':"user4", 'password':"44444", 'count':1})
        respData = self.makeRequest("/users/login", method="POST", data = { 'user' : 'user4', 'password' : '77777777'} )
        self.assertResponse(respData, count = None, errCode = testLib.RestTestCase.ERR_BAD_CREDENTIALS)


