from django.conf.urls import patterns, include, url
# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'solutorie.views.home', name='home'),
    # url(r'^solutorie/', include('solutorie.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
    #(r'^login/$', 'auth.views.login_user'),
    #(r'^users/$', UserCounter_HTTPRequestHandler),
    #(r'^TESTAPI/$', TESTAPI_Controller),
    url(r'^users/login', 'auth.views.login'),
    url(r'^users/add', 'auth.views.add'),
    url(r'^TESTAPI/resetFixture', 'auth.views.reset'),
    url(r'^TESTAPI/unitTests', 'auth.views.unitTest'),
    url(r'^', 'auth.views.main'),
)
